from django.db import models

# Create your models here.


class UploadFile(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False, help_text='Name of the Excel File')
    datafile = models.FileField(upload_to='input/excel/')
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-date_created']


class SectionFile(models.Model):
    section_name = models.CharField(max_length=255, blank=False, null=False, help_text='Name of the Excel File')
    datafile = models.FileField(upload_to='wla/image/')
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return self.section_name

    class Meta:
        ordering = ['-date_created']


class Lookahead(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False, help_text='Name of the Excel File')
    datafile = models.FileField(upload_to='lookahead/excel/')
    status_date = models.DateTimeField(null=True, blank=True)
    look_back_day=models.IntegerField(default=7)
    look_ahead_day = models.IntegerField(default=21)
    date_created = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-date_created']