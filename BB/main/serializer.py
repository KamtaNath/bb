from .models import UploadFile,Lookahead,SectionFile
from rest_framework import serializers
from django_pandas.io import read_frame
from ast import literal_eval
import pandas as pd
import json
from datetime import datetime
from datetime import timedelta


class SectionFileSerializer(serializers.ModelSerializer):
    class Meta:
        model=SectionFile
        fields = '__all__'


class LookaheadSerializer(serializers.ModelSerializer):
    excel_data = serializers.SerializerMethodField()
    class Meta:
        model=Lookahead
        # fields = '__all__'
        fields = (
            'id',
            'name',
            'datafile',
            'look_back_day',
            'status_date',
            'look_ahead_day',
            'date_created',
            'excel_data'
        )

    def get_excel_data(self,obj):
        status_date = obj.status_date
        prev_date = (status_date -timedelta(days=obj.look_back_day)).strftime('%Y-%m-%d')
        next_date = (status_date +timedelta(days=obj.look_ahead_day)).strftime('%Y-%m-%d')

        xl = pd.ExcelFile(obj.datafile)
        df = pd.read_excel(xl, sheet_name=1)
        df = df[['St Date','End Date','Task ID','Task Description','3WLA Task Description','Resources','Quantity','Chainage From','Chainage To']]
        # df =df.dropna()
        s =df['St Date']

        lock = df[(s > pd.to_datetime(prev_date)) & (s < pd.to_datetime(next_date))]
        df = lock

        df['St Date'] = df['St Date'].dt.strftime('%Y-%m-%d')
        df['End Date'] = df['End Date'].dt.strftime('%Y-%m-%d')
        count = df['Task ID'].count()
        print(count)
        print(df)





        return {
            'count':count,
            'section_data': json.loads(df.to_json(orient='records'))
            # 'section_data': "test"

        }




class UploadSerializer(serializers.ModelSerializer):
    excel_data = serializers.SerializerMethodField()
    class Meta:
        model = UploadFile
        fields = (
            'id',
            'name',
            'datafile',
            'date_created',
            'excel_data'

        )

    def get_excel_data(self, obj):
        # print(obj.datafile)
        xl = pd.ExcelFile(obj.datafile)
        df = pd.read_excel(xl, sheet_name=0)
        df1 = df[['Section Name', 'Section Define', 'From Chainage', 'To Chainage']]
        df2 =df[['Start Chainage', 'Finish Chainage', 'Section Size']].dropna()
        df2['No of columns'] = (df['Start Chainage'] - df['Finish Chainage'])/(df['Section Size'])

        # df2=df2.dropna()
        # print("df2",df2)
        # df = df.loc[:, ~df.columns.str.contains('^Unnamed')]

        return {
          'base_data': literal_eval(df2.to_json(orient='records')),
          'section_data':literal_eval(df1.to_json(orient='records'))

        }