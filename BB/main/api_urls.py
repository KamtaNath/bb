from django.urls import path
from django.conf.urls import include,url
from .views import *
from rest_framework.routers import DefaultRouter,SimpleRouter


router = DefaultRouter()
router.register('input',UploadDocFileViewSet)
router.register('wla',LookAheadViewSet)
router.register('sectionImage',SectionFileViewSet)


urlpatterns = [
    path('',include(router.urls)),


]