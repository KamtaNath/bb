from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets
from .models import UploadFile,Lookahead,SectionFile
from .serializer import UploadSerializer,LookaheadSerializer,SectionFileSerializer
# from django_pandas.io import read_frame
import pandas as pd
from rest_framework.exceptions import APIException


class SectionFileViewSet(viewsets.ModelViewSet):
    serializer_class = SectionFileSerializer
    queryset = SectionFile.objects.all()
    lookup_field = 'id'


class LookAheadViewSet(viewsets.ModelViewSet):
    serializer_class = LookaheadSerializer
    queryset = Lookahead.objects.all()
    lookup_field = 'id'


class UploadDocFileViewSet(viewsets.ModelViewSet):
    serializer_class = UploadSerializer
    queryset = UploadFile.objects.all()
    lookup_field = 'id'

    # def list(self, request):
    #     queryset = UploadFile.objects.all()
    #     serializer = UploadSerializer(queryset, many=True)
    #     print("inside list-------------->>>>>>>>>>>> ",type(serializer.data))
    #     content ="hello world"
    #     # return Response(content)
    #     # serializer.data['item']='test'
    #     return Response(serializer.data)

    def perform_create(self, serializer):
        xl = pd.ExcelFile(self.request.data.get('datafile'))
        # print(xl.sheet_names[0])
        # sheet = pd.read_excel(xl, sheet_name=0)
        # print("no of --",dfshape)
        # print(df.head(20))
        # print("-----------------------------------------------------")
        # sheet= pd.read_excel(xl,sheet_name=0,index_col=0)
        # print(df.head(5))
        # print("-----------------------------------------------------")
        # sheet= pd.read_excel(xl,sheet_name=0,index_col=1)
        # print(df.head(5))
        # print("-----------------------------------------------------")
        # sheet= pd.read_excel(xl,sheet_name=0,index_col=2)
        # print(df.head(5))
        df = pd.read_excel(xl, sheet_name=0)
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # df = df.dropna(inplace=True)
        # print("-----------------Column names------------------------------------")
        # print(df.columns)
        print("Start Chainage count",(df['Start Chainage']).count())
        print("-----------------------------------------------------")


        df['No of columns'] = (df['Start Chainage'] - df['Finish Chainage'])/(df['Section Size'])

        if (df['Section Name']).count() == (df['Section Define']).count() ==(df['From Chainage']).count() ==(df['To Chainage']).count() and (df['Start Chainage']).count()==(df['Start Chainage']).count()==1:

            print("Valid data")
            print(df.head(50))
            content = "Invalid data type"
            serializer.save()


        else:
            content="Invalid data type"
            raise APIException(content)

        # print((df['Section Name']).count())




