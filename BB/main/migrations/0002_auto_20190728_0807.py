# Generated by Django 2.2.3 on 2019-07-28 08:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lookahead',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='Name of the Excel File', max_length=255)),
                ('datafile', models.FileField(upload_to='lookahead/excel/')),
                ('status_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('look_back_day', models.IntegerField(blank=True, default=7, null=True)),
                ('look_ahead_day', models.IntegerField(blank=True, default=21, null=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, null=True)),
            ],
        ),
        migrations.RenameField(
            model_name='uploadfile',
            old_name='date',
            new_name='date_created',
        ),
        migrations.AlterField(
            model_name='uploadfile',
            name='datafile',
            field=models.FileField(upload_to='input/excel/'),
        ),
    ]
